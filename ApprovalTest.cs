﻿using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace csharp
{
    [TestFixture]
    public class ApprovalTest
    {
        [Test]
        public void ThirtyDays()
        {
            var lines = File.ReadAllLines("C:\\Users\\rodrigad\\Documents\\Visual Studio 2015\\Projects\\GildedRose-Refactoring-Kata\\ThirtyDays.txt");

            StringBuilder fakeoutput = new StringBuilder();
            Console.SetOut(new StringWriter(fakeoutput));
            // Console.SetIn(new StringReader("a\n"));

            Program.Main(new string[] { });
            String output = fakeoutput.ToString();

            var outputLines = output.Split('\n');
            for(var i = 0; i<Math.Min(lines.Length, outputLines.Length); i++)
            {
                outputLines[i] = outputLines[i].TrimEnd('\r');
                Assert.AreEqual(lines[i], outputLines[i]);
            }
        }
    }
}
