﻿using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        private int _currentIndex;
        readonly IList<Item> Items;
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality()
        {
            for (var i = 0; i < Items.Count; i++)
            {
                var currentItem = GetCurrentItem(i);

                if (IsSulfuras())
                    continue;

                UpdateQualityByType();

                UpdateSellIn();

                UpdateQualityBySellIn(currentItem);
            }
        }

        private Item GetCurrentItem(int i)
        {
            Item currentItem = Items[i];
            _currentIndex = i;
            return currentItem;
        }

        private bool IsSulfuras()
        {
            return Items[_currentIndex].Name == "Sulfuras, Hand of Ragnaros";
        }

        private void UpdateQualityByType()
        {
            Item currentItem = Items[_currentIndex];

            if (IsAgedBrie() || IsBackstagePassesToTafkal())
            {
                if (!QualityUnderFifty()) return;

                currentItem.Quality++;

                if (!IsBackstagePassesToTafkal()) return;

                if (SellInUnderEleven())
                {
                    if (QualityUnderFifty())
                    {
                        currentItem.Quality++;
                    }
                }

                if (SellInUnderSix())
                {
                    if (QualityUnderFifty())
                    {
                        currentItem.Quality++;
                    }
                }
            }
            else if (IsConjured())
            {
                if (PositiveQuality())
                {
                    currentItem.Quality = currentItem.Quality - 2;
                }
            }
            else
            {
                if (PositiveQuality())
                {
                    currentItem.Quality--;
                }
            }
        }

        private bool IsAgedBrie()
        {
            return Items[_currentIndex].Name == "Aged Brie";
        }

        private bool IsBackstagePassesToTafkal()
        {
            return Items[_currentIndex].Name == "Backstage passes to a TAFKAL80ETC concert";
        }

        private bool QualityUnderFifty()
        {
            return Items[_currentIndex].Quality < 50;
        }
        private bool SellInUnderEleven()
        {
            return Items[_currentIndex].SellIn < 11;
        }

        private bool SellInUnderSix()
        {
            return Items[_currentIndex].SellIn < 6;
        }

        private bool IsConjured()
        {
            return Items[_currentIndex].Name == "Conjured";
        }

        private bool PositiveQuality()
        {
            return Items[_currentIndex].Quality > 0;
        }

        private void UpdateSellIn()
        {
            Item currentItem = Items[_currentIndex];
            currentItem.SellIn--;
        }

        private void UpdateQualityBySellIn(Item currentItem)
        {
            if (PositiveSellin()) return;

            if (IsAgedBrie())
            {
                if (QualityUnderFifty())
                {
                    currentItem.Quality++;
                }
            }
            else if (IsBackstagePassesToTafkal())
            {
                currentItem.Quality = 0;                   
            }
            else if (IsConjured())
            {
                if (PositiveQuality())
                    {
                        currentItem.Quality = currentItem.Quality - 2;
                    }
            }
            else
            {
                if (PositiveQuality())
                {
                    currentItem.Quality--;
                }
            }
        }
        
        private bool PositiveSellin()
        {
            return Items[_currentIndex].SellIn >= 0;
        }
        
    }
}
