﻿using NUnit.Framework;
using System.Collections.Generic;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        [Test]
        public void foo()
        {
            IList<Item> Items = new List<Item> { new Item { Name = "foo", SellIn = 0, Quality = 0 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual("foo", Items[0].Name);
        }

        [Test]
        public void should_degrade_quality_of_conjured_item_twice_as_normal_item()
        {
            IList<Item> Items = new List<Item>
            {
                new Item { Name = "foo", SellIn = 10, Quality = 10 },
                new Item { Name = "Conjured", SellIn = 10, Quality = 10}
            };

            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();

            var normalItemQuality = Items[0].Quality;
            var conjuredItemQuality = Items[1].Quality;

            Assert.AreEqual(conjuredItemQuality+1, normalItemQuality);
        }
    }
}
